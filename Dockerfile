FROM alpine:3.8

#ENV VERSION 3.3.1
#ENV REVISION b72d67b
ENV VERSION 3.4.1
ENV REVISION efbcc65

RUN apk add --no-cache mariadb-dev postgresql-dev unixodbc-dev mysql-client && \
    mkdir -p /app/etc /app/nubex /app/logs && \
    chmod -R 777 /app && \
    ln -sf /dev/stdout /app/logs/out.log && \
    ln -sf /dev/stderr /app/logs/err.log && \
    wget -q http://sphinxsearch.com/files/dicts/ru.pak && \
    wget -q http://sphinxsearch.com/files/dicts/en.pak && \
    mv *.pak /app/etc/ && \
    wget -q http://sphinxsearch.com/files/sphinx-${VERSION}-${REVISION}-linux-amd64-musl.tar.gz && \
    tar zxf sphinx-${VERSION}-${REVISION}-linux-amd64-musl.tar.gz && \
    mv sphinx-${VERSION}/bin/* /usr/local/bin/ && \
    rm -rf sphinx-${VERSION} && \
    rm sphinx-${VERSION}-${REVISION}-linux-amd64-musl.tar.gz

COPY ./sphinx.conf /app/etc/
VOLUME ["/app/nubex", "/app/logs"]
STOPSIGNAL SIGKILL
EXPOSE 9306
CMD ["searchd", "--nodetach", "-c", "/app/etc/sphinx.conf"]
